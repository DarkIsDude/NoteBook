- `ln -s /Users/darkisdude/Workspaces/notebook.git/zsh/zsh.rc .zshrc`

My theme : `https://draculatheme.com/zsh`

In Warp, open the theme picker with `Cmd + P` and search for dracula.

## Using npm with zsh

Link of the [github issue](https://github.com/npm/cli/issues/5153).

```bash
npm config set script-shell zsh
# And set the alias in .zshenv
alias whatthefuck='script-shell zsh'
```
