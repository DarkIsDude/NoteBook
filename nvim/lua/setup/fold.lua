local vim = vim
local opt = vim.opt
local keyset = vim.keymap.set

opt.foldenable = true
opt.foldmethod = "syntax" -- syntax highlighting items specify folds
opt.foldcolumn = "1" -- defines 1 col at window left, to indicate folding
opt.foldlevelstart = 99 -- start file with all folds opened
opt.foldlevel = 99

keyset('n', '<leader>zc', 'zM<CR>')
keyset('n', '<leader>zo', 'zR<CR>')

vim.api.nvim_create_autocmd({'BufEnter', 'BufAdd', 'BufNew', 'BufNewFile', 'BufWinEnter'}, {
    group = vim.api.nvim_create_augroup('TS_FOLD_WORKAROUND', {}),
    callback = function()
        opt.foldenable = true
        opt.foldmethod = "syntax" -- syntax highlighting items specify folds
        opt.foldcolumn = "1" -- defines 1 col at window left, to indicate folding
        opt.foldlevelstart = 99 -- start file with all folds opened
        opt.foldlevel = 99
        -- vim.opt.foldexpr       = 'nvim_treesitter#foldexpr()'
    end
})
