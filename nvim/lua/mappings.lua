local keyset = vim.keymap.set

-- Leader key
vim.g.mapleader = ' '

-- Switch between buffer
keyset('n', '<TAB>', ':bn<CR>')
keyset('n', '<S-TAB>', ':bp<CR>')
keyset('n', '<leader>w', ':bp<bar>sp<bar>bn<bar>bd<CR>')

-- Remap split management
keyset('n', '<leader>ww', ':wincmd w<CR>')
keyset('n', '<leader>wc', ':wincmd c<CR>')
keyset('n', '<leader>wq', ':wincmd q<CR>')
keyset('n', '<leader>wv', ':wincmd v<CR>')
keyset('n', '<leader>ws', ':wincmd s<CR>')

keyset('t', '<esc>', '<C-\\><C-N>') -- exit terminal mode
