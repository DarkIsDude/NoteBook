return {
{
    'rcarriga/nvim-dap-ui',
    dependencies = {"mfussenegger/nvim-dap", "nvim-neotest/nvim-nio"},
    config = function()
        local dap = require("dap")
        local dapui = require('dapui')

        vim.keymap.set('n', '<leader>dl', dap.continue, {
            desc = "Lauch"
        })
        vim.keymap.set('n', '<leader>db', dap.toggle_breakpoint, {
            desc = "Breakpoint"
        })
        vim.keymap.set('n', '<leader>dt', dap.terminate, {
            desc = "Terminate"
        })
        vim.keymap.set('n', '<leader>ds', dap.step_over, {
            desc = "Step Over"
        })
        vim.keymap.set('n', '<leader>di', dap.step_into, {
            desc = "Step Into"
        })

        dap.adapters.node2 = {
            type = 'executable',
            command = 'node',
            args = {os.getenv('HOME') .. '/Workspaces/vscode-node-debug2.git/out/src/nodeDebug.js'}
        }

        dap.configurations.typescript = {{
            type = "node2",
            request = "attach",
            name = "Auto Attach",
            cwd = vim.fn.getcwd()
        }, {
            type = "node2",
            name = "Docker attach API",
            request = "attach",
            remoteRoot = "/usr/src/app",
            localRoot = "${workspaceFolder}",
            port = 9229
        }, {
            type = "node2",
            name = "Docker attach Worker",
            request = "attach",
            remoteRoot = "/usr/src/app",
            localRoot = "${workspaceFolder}",
            port = 9230
        }}
        dap.configurations.javascript = dap.configurations.typescript

        dapui.setup()
        dap.listeners.before.attach.dapui_config = function()
            dapui.open()
        end
        dap.listeners.before.launch.dapui_config = function()
            dapui.open()
        end
        dap.listeners.before.event_terminated.dapui_config = function()
            dapui.close()
        end
        dap.listeners.before.event_exited.dapui_config = function()
            dapui.close()
        end
    end
}}
