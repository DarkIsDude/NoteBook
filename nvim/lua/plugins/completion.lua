return {{
    'neoclide/coc.nvim',
    branch = 'release',
    dependencies = {'fannheyward/coc-marketplace'},
    config = function()
        require('setup/coc')
    end
}, {
    'sheerun/vim-polyglot',
    config = function()
        require('setup/fold')
    end
}, {
    'nvim-treesitter/nvim-treesitter',
    build = ':TSUpdate'
}, {
    'CopilotC-Nvim/CopilotChat.nvim',
    dependencies = {'github/copilot.vim', 'nvim-lua/plenary.nvim'},
    build = "make tiktoken",
    opts = {
        debug = true
    }
}, {'tpope/vim-commentary'}}
