return {{
    'dracula/vim',
    dependencies = {'ryanoasis/vim-devicons'},
    config = function()
        vim.cmd [[colorscheme dracula]]

        -- dev icon
        local gset = vim.api.nvim_set_var

        gset('airline_powerline_fonts', 1)
        vim.opt.encoding = 'utf8'

        -- Set the background of the terminal to transparent
        vim.cmd [[
            highlight Normal guibg=none
            highlight NonText guibg=none
            highlight Normal ctermbg=none
            highlight NonText ctermbg=none
        ]]
    end
}, {
    'vim-airline/vim-airline',
    config = function()
        vim.api.nvim_set_var('airline#extensions#tabline#enabled', 1)
    end
}, {'mhinz/vim-startify'}}
