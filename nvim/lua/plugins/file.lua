return {{
    'preservim/nerdtree',
    config = function()
        vim.keymap.set('n', '<leader>np', ':NERDTreeToggle<CR>')
        vim.keymap.set('n', '<leader>nn', ':NERDTreeFind<cr>')
    end
}, {'preservim/tagbar'}, {
    'nvim-telescope/telescope.nvim',
    dependencies = {'nvim-lua/plenary.nvim', 'nvim-telescope/telescope-live-grep-args.nvim'},
    config = function()
        local keyset = vim.keymap.set
        local builtin = require('telescope.builtin')
        local telescope = require('telescope')

        telescope.load_extension('live_grep_args')

        keyset('n', '<leader>ff', ':Telescope find_files<CR>', {})
        keyset('n', '<leader>fg', builtin.live_grep, {})
        keyset('n', '<leader>fb', builtin.buffers, {})
        keyset('n', '<leader>fh', builtin.help_tags, {})
        keyset('n', '<leader>fc', builtin.commands, {})
        keyset('n', '<leader>fgb', builtin.git_branches, {})
        keyset('n', '<leader>fs', telescope.extensions.live_grep_args.live_grep_args, {})

        telescope.setup {
            pickers = {
                find_files = {
                    --                  hidden = true,
                },
                live_grep = {
                    additional_args = function(opts)
                        return {
                            "--hidden",
                            "--no-ignore-vcs",
                            "-g", "!**/node_modules/*",
                            "-g", "!**/.git/*",
                            "-g", "!**/.cache/*",
                            "-g", "!**/.repro/*",
                            "-g", "!**/dist/*",
                            "-g", "!**/.terragrunt*",
                            "-g", "!**/.terraform*",
                            "-g", "!**/android*",
                            "-g", "!**/ios*"
                        }
                    end
                }
            }
        }

    end
}, {
    'hedyhli/outline.nvim',
    config = function()
        vim.keymap.set('n', '<leader>gs', '<cmd>Outline<CR>', {
            desc = 'Toggle Outline'
        })
        require('outline').setup({})
    end
}}

