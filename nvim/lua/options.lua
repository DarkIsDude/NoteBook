vim.o.expandtab = true
vim.o.nu = true
vim.o.rnu = true
vim.o.shiftwidth = 2
vim.o.softtabstop = 2
vim.o.tabstop = 2
vim.o.syntax = 'on'
vim.o.updatetime = 200
vim.o.termguicolors = true
vim.opt.clipboard = 'unnamedplus'
vim.o.confirm = true
vim.o.scrolloff = 8
vim.o.sidescrolloff = 8
vim.o.signcolumn = 'yes'
vim.o.wildmenu = true
vim.o.wildmode = 'longest,full:full'

