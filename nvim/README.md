# NVIM Configuration

`ln -s /Users/darkisdude/Workspaces/notebook.git/nvim/ .config/nvim`

Don't forget to install brew.

You also need to install all Coc dependecies with

```sh
CocInstall coc-xml coc-html coc-css coc-docker coc-eslint coc-git coc-go coc-json coc-markdownlint coc-prettier coc-python coc-sh coc-sql coc-tsserver coc-pyright coc-biome
```

You need to install and change the policy of iTerm with `JetBrains one (Nerd Edition)`.

Install ripgrep with `brew install ripgrep`.

Configure your debugger with https://github.com/mfussenegger/nvim-dap/wiki/Debug-Adapter-installation#node-debug2.

## Terraform

https://github.com/hashicorp/terraform-ls?tab=readme-ov-file

https://www.braybaut.dev/posts/integrate-terraform-language-server-protocol-with-vim/

## Cheatsheet

### General

- `:%s/<old>/<new>/gc` search and replace old with new globally and asking for confirmation

### Neovim

- `ctrl+ws` - Split windows horizontally
- `ctrl+wv` - Split windows vertically

- `gd` jump to the code definition
- `gy` jump to the type definition
- `gi` jump to the code implementation
- `gr` show code references
- `K` to show documentation
- `[g` / `]g` navigate diagnostics
- `<leader>a` to show all diagnostics
- `<leader>qf` apply the first quickfix
- `<leader>f` format the selected code

- `<leader>o` open the outline

- `<leader>zc` close all fold (zM)
- `<leader>zo` open all fold (zR)
- `zc / zo` close / open the current fold

- `<leader>fs` search text with args
  - `--hidden file` to search in hidden file
  - `-g "!<file>"` to exclude file
  - `-g "!<file>"` to exclude file
  - `--no-ignore` to don't ignore any file
  - `-t type` to search only file type
- `<leader>fh` help tags search
- `<leader>fc` commands search
- `<leader>fgb` git branches search
