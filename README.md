# La partie WIKI est utilisé ici pour sauvegarder des informations

You can create symbolic link like that:

- `ln -s /Users/darkisdude/Workspaces/notebook.git/gitconfig .gitconfig`
- `ln -s /Users/darkisdude/Workspaces/notebook.git/warp/ .warp`
- `ln -s /Users/darkisdude/Workspaces/notebook.git/karabiner/ .config/karabiner`

`brew install neovim`

Install the aws cli.

## Helix editor

The config is inside `.config/helix/`

### Bash

npm i -g bash-language-server

### CSS / HTML / JSON

npm i -g vscode-langservers-extracted

### Docker

npm install -g dockerfile-language-server-nodejs

npm install -g @microsoft/compose-language-service

### Go

go install golang.org/x/tools/gopls@latest                            # LSP
go install github.com/go-delve/delve/cmd/dlv@latest                   # Debugger
go install golang.org/x/tools/cmd/goimports@latest                    # Formatter
go install github.com/nametake/golangci-lint-langserver@latest        # Linter
go install github.com/golangci/golangci-lint/cmd/golangci-lint@latest # Linter cli

### GrapphQL

npm i -g graphql-language-service-cli

### Terraform

Install terraform-ls

### SQL

npm i -g sql-language-server

### Typescript

npm install -g typescript typescript-language-server

### Eslint

npm i -g vscode-langservers-extracted@4.8

https://github.com/helix-editor/helix/wiki/Language-Server-Configurations#eslint

## Backup

Before reset don't forget to:

- Extract MoneyWiz
- Get back the photos folder
